## Engineering Notebook
Arely Irra 
ECE 445
SP 2023

## 2/7/23 Meeting
We had our first TA meeting today. I wrote down all the notes from the meeting. Our proposal is due this week.

## 2/9/23 Proposal
Worked on last minute edits to the proposal. 

## 2/14/23 Meeting
Meeting with TA
TA Notes:
Meetings are informal, if we decide that there is no need for meeting we can decide to not meet just send the TA the message
Pulling power out of the sockets to charge the inductance (constant pull) 
If you’re pulling any voltage from the thing we need to do extra training!! We need this documentation 
If we pull electricity from sockets its considered high voltage so might need training. 
The thing is, dont do copper windings ourselves! (spend the 2 bucks) 
Add capacitance to make it more stable? When you charge discharge, stuff wears off, so by doing 10 to 12 mockups, all of sudden things don’t work that well or as expected
Adding voltage regulator
https://www.digikey.com/en/products/detail/diodes-incorporated/AP7366-W5-7/9867323
https://www.digikey.com/en/products/detail/richtek-usa-inc/RT9043GB/2470046
https://www.digikey.com/en/products/detail/diodes-incorporated/AP7331-WG-7/2270813
https://www.digikey.com/en/products/detail/diodes-incorporated/AP7361-FGE-7/2594049

Inductors got phased out commercially
We can test the inductance (how well it works) before using it
Voltage regulators that work with inductance (look into this, TA unsure)
Worried about the flimsy manufacturing, so add the voltage regulator to not burn any components
Embedded experience, or doubts, email TA with time 
5V max, 
Input into microcontroller, one output from the microcontroller
https://www.digikey.com/en/products/detail/nxp-usa-inc/NT3H1201W0FTTJ/5347878 (input)
MLX90109 - rfid reader that seems promising

Motor with an encoder
How to check if they have encoders: TA can check if they have any motors in the back with encoders) Our TA can do this or other TA’s can do this too. 
Motor housing: how do we plan to make sure it stays there? Deadbolt 
For demo, prob use a square cut out slab of wood, ask machine shop for mounting input.
Next big thing is the design document due Feb 23rd, and then PCB 
Design: proper design document
Once PCB gets designed, its about a 4 week wait, within that 4 week wait, perfect the rest of the design
 We can do the mock-up rather than wait for the machine shop, use cardboard instead to mount our project 
Take mockup to machine shop for their input/advice and so they have a good idea

## 2/21/23 Design Doc
Design document work. Read over IEEE ethics code and added what directly applied to our project to the document. Since most of the requirements for the design document come from our proposal, I made sure to add from our proposal to the design document but with revisions to the subsystems. Made sure to create a realistic timeline for our project schedule as well.

## 2/23/23 
For the motor, we need to know how much force a deadbolt would require to be turned. So I went to the shop planning on measuring the force it took to turn it. When I talked to the shop however they told me that they often work with this style of project for other classes and they already had a door with a deadbolt on it. The motors they use for it are more than 3kg/cm, but that a 3kg/cm motor would work in the optimal conditions. Because our design is meant to open even if not in optimal conditions, he suggested we use a 20kg/cm servo motor. That way the deadbolt will open even in the worst conditions. 

## 2/27/23 Design Doc
Had the design review today. We received a lot of feedback including adding voltages to our block diagram and as more details to the subsystems and tolerance analysis.

## 3/1/23: Initial testing
I looked over my teammates notes and falstad simulation for the power circuit. Everything looked good to me. It looks like we’re going to need a higher power rated coil than originally planned and a different voltage mosfet in order to get at least 5V across the circuits. 

## 3/02/23: Schematic Talk
We talked about starting schematics and who’s going to work on what this week. Since we tested the power circuit that would transfer power by induction, I’ll be starting on working on that in kicad, the PCB software program. David said he could work on the schematic for the microcontroller and motor. I also quickly worked out revisions with my team to our proposal for our subsystem and safety requirements. 

## 3/6/23: PCB Design
Started schematic for PCB design for the power transceiver. Made sure all the parts were instock through digikey. The design in the design document for the power transceiver circuit includes a parallel branch with an LED and resistor for debugging purposes. I talked to my teammates and we opted it out of our PCB design since it’s not necessary for the functionality of the design for the final product. 
While creating the schematic I realized that the inductor coils in the circuit from the design document have three connections however inductor coils we listed only have two connections. After discussing with my teaamatesx we decided to have our PCB design follow the schematic and we would create a third connection to our coils. I could not find footprints for the inductor coils Nich found so I opted to use resistor 0805 2012 metric size footprints instead and I can solder the wires to the padding. 
I finished the schematic and pushed it to the repo. 

## 3/07/23: PCB Design
I did more research on the third connection to the coil and it looks like its for power to flow to ground, through the gate, and through the circuit. Originally the footprint for the inductor coils were going to be just generic resistor surface mount pads, however the coils in the datasheet and the footprints for the 0805 2012 metric footprints were not large enough for the coils so I opted to use a three input molex connector footprint that was a through-hole mount instead because the diameter of the holes would be large enough for the thickness of the coils to go through. One pin would connect the inductor coil to the circuit, the second pin would be connected to the power input on the pcb and the second inductor coil wire, and the last hole would connect to the mosfet and capacitor on the pcb and the last wire on the inductor coil.
I updated the schematic and the PCB files to reflect these changes and uploaded them to discord. I asked my teammates to review my work incase I missed something.

## 3/08/23: PCB ordering
Parts have been ordered for the power transistor circuit to be tested on breadboard while the PCB arrives. I verified with my teammates that this was the design we wanted for sure for our power transmitter circuit. I did the PCB DRC check through kicad and fixed a few grounding errors I hadn’t seen before. I tried making the PCB as space efficient as possible as this would be the unit the user would be carrying, and smaller space means less cost in materials to produce. I made sure to checkoff everything off of the checklist given to us from the course website and email on how to order PCBs. I sent the gerber zip file to be evaluated by PCBway, and after a long time, it got approved. 
However as I was looking over the PCB I realized that a lot of my tracks were 10 mils. During the CAD assignment at the beginning of the course they explained that most components dealing with high power need tracks of about 20 mils. Since this is a power circuit I went back and made sure most components had 20 mils. I then went through running the DRC, sending it to PCBway for review and getting it approved. I sent the approved files to the TA and he confirmed that the PCB’s are on the list to be ordered tomorrow morning.

## 3/09/23 
I attached the kicad schematic files and the gerber files to my branch on the repo. 


## 3/20/23 Discussion
Team talked about a few updates today. The parts for the circuit for breadboard testing arrived, however the mosfet that arrived was incredibly small, about 2 millimeters by 2 millimeters which is not realistically usable for breadboarding. The mosfets in the lab are also not the specs that we need as they’re really high ratings for 120V, 70V, and we only need about 10V max. Without the mosfet we can’t fully test our circuit. Nich and I agreed to do more research and report if we find any parts that can be large enough to be breadboarded but that also have a lower voltage rating. 

## 3/26/23 Mosfet
After more research on Mosfets, we realized that the voltage rating isn’t about the voltage it needs to work but more so about the voltage that will burn out or make the part not work properly. I wasn’t able to find any mosfets that were hand solderable or breadboardable at 5V so knowing that we can use some at 60V, we decided to buy one from the ECE shop to use in our circuit. 

## 3/27/23 Small Parts

For testing the power receiver circuit we had ordered a linear regulator. For parts that we couldn’t get from the lab but wanted to test on the breadboard we ordered them online and decided we’d solder wires to the components. However after double checking the datasheet we saw that the part was too small for that idea. The widths of the wires from our lab kits were the same size as the whole component together. If we wanted to use it for the PCB we would need to oder a stencil for it. Since this was the second time we’d ordered a part too small, I sent a reference to the rest of the team that the size for metal legs should at least be around 1.2 millimeters by 1.2 millimeters. This way for any future part orders we’re all aware if its breadboardable, hand solderable, or would need stencils. 

## 3/28/23 Small parts

Nich sent me a datasheet with a few linear regulators that we could use. I specified which ones would work. Specifically the  AZ1117I where the smallest widths of the pads on the footprint were about 1.2 mm so we went with using that one.

## 3/30/23 PCB V2
I started the PCB for the schematic David made on the microcontoller and it’s components. I started to build the PCB but wanted to have the correct footprints. As I was looking for parts like capacitors and resistors I found that there wasn’t a resistor at 19k ohms that was in stock. I did find a 19.1k resistor however and I let the team know about this small change. I wrote down all the parts I had found in stock and their footprints. However there were some components in the schematic that didn’t have specific values so I contacted my teammate for help in filling in the values. 

RFID-Lock Parts for soldering:

R1-100k ohm resistor: RCC1206150RFKEA Vishay Dale | Resistors | DigiKey
R2->19kOhsm resistor:
MCU1-> STM32F103C8T6:STM32F103C8T6 STMicroelectronics | Integrated Circuits (ICs) | DigiKey
C1-> 100nF capacitor::
RFID1-> MLX90109:
L1 (MIssing value info)
CD1: 100nF ->
C3 (missing value info)->
R3 -> 100k 
R4-> 100k
R5->1k
R6->1k
J1->REd_led : same parts as class
J2 Green_Led: class parts
J4 DisableBin :class part?
R7 10k resistor 
C4 (value?
J3 EnableBIn 
R8 10k 
C2 (value?)

Transceiver: 
Diodes: 1N4001 
Capacitance, reg one from class
Polar Capacitance 100uF: ESK107M025AE3KA KEMET | Capacitors | DigiKey,ESK107M035AE3AA KEMET | Capacitors | DigiKey
^^“” but 10uF: C322C106K3R5TA KEMET | Capacitors | DigiKey :downloaded footprint for it
AMS1117 -> we chose AZ1117 instead for soldering reasons: AZ1117I (diodes.com)
  

## 3/03/23 PCB V2

The diodes specified in the schematic for the power receiver circuit call for 1N4001 diodes but digikey did not have them available: https://www.digikey.com/en/products/detail/onsemi/1N4001/976982#product-details-substitutes
They did, however have suggestions for substitutes and I found the 1N4001RLG instead:https://www.digikey.com/en/products/detail/onsemi/1N4001/976982#product-details-substitutes,
The parts are almost identical and should work for our purposes. 

## 3/04/23 PCB Changes
I had most of the PCB file complete, however my teammate realized we would needed an antenna and tuning circuit added to the schematic. These were big changes that would take time to add. While my teammate made the changes to the schematic I found the extra parts on digikey that were added to the circuit and made the power receiver circuit schematic.

## 3/05/23 PCB Changes Cont.
I added the footprints to the new schematic for the microcontroller. I also added the power receiver circuit schematic to this file as the power source of the microcontroller comes from that circuit directly. This makes it one big PCB. 

Our TA warned us about using parts too small to hand solder however the NFC Reader IC is a specific part that we can’t switch out or replace so we know already that we would need stencil for that part. The microcontroller might also be really hard to solder by hand so these are two components already that we need to be careful when soldering. 

This is the size comparison between the microcontroller and standard 0805 2012 metric capacitor. Some of the capacitors in the schematic were labeled as NC, and so they act as an open wire so I did not add their footprints to the PCB. 

## 3/06/23 PCB Erros
I connected the components together and organized them so that they would be as close as possible to save space as there were a lot of components in this PCB. I ran DRC but had issues with some of the footprints. I had downloaded a few EDA models from digikey however they had intersecting lines in kiCAD which produced warnings. I went ahead adjusted the lines through the footprint creator in kiCAD so that the lines drawn would not intersect with each other and double checked through the datasheets that the padding was still the same size as the recommended footprints on the sheets. 

One of my teammates informed me that they already bought two parts for the power receiver circuit. These were capacitors, and the 100uF capacitor footprint and part that I had in mind was not the same as they had already bought. I quickly made that change to the footprint to fit the right size and footprint style on the schematic. Before I had a polarized 100uF through hole mounted capacitor and my teammate had already gotten a surface mount capacitor. 

After everything was connected, I still had an issue. My grounds were not connected to each other. I had put vias where I thought it needed it most near the air wires and still the airwires for the ground connections would not disappear.
My initial thought was that my components were all too close together so the components wouldn’t be able to properly be placed on the board. So i started giving the components some clearance hoping that would help fix any issues and refilled the zones. While doing this I noticed that my vias sometimes read 3+ and sometimes read Ground. I was confused on this and after doing research I could not find much information about how to fix this issue. I contacted my TA for any guidance they had on identifying the issue.

The issue was that my ground planes were not a ground planes but they were both +3V planes. As soon as I changed them to GND planes the vias all displayed “GND” and the airwires disappeared.

## 3/07/23 PCBway
Circuit cleared DRC and I made sure to include the stencil files in the gerber zip file. The gerber zip file passed as well through PCBway and I sent the files to the TA in hopes of getting the order in as soon as possible. 
   

## 3/09/23 Ordering Parts
I ordered the new parts for the PCB including the Diodes, resistors, crystal, capacitors, microcontroller and NFC Reader, and resistors with higher wattage for the power transmitter circuit.

## 3/18/23: 
I soldered part of the power transceiver circuit and realized I forgot to order R 5k ohm so I ordered that part with fast delivery. I met up with Nich to discuss how our pcb needs to be updated and how our current design can be changed to our new design. It looks like our PCB for the power transceiver circuit would need a lot fo updates that can’t be fixed on the PCB as it is. We would need to re-design and print a PCB. We took note of this and decided that because our PCB original design was not up to date with the additions and tweakings we’ve made while debugging, that we would have to focus on integrating the breadboard working circuit with the new PCB coming in for the power receiver and microcontroller circuit.

## 3/20/23 Soldering
The PCB and stencil arrived and so did our parts. I started by applying the stencil to the PCB, applying solder paste, and playing it in the oven. It took a few tries before I was satisfied with the placing and amount of solder paste on the PCB since it was difficult to keep the stencil aligned with the PCB while adding the solder paste. I focused on only using the paste to connect the components we can’t hand solder, and then hand soldered the rest. While I was hand soldering I realized that we didn’t have the crystal. We had a crystal with 2 pads but we our design required a crystal with 4 pads. This component helps the chip look for the RFID tag signal and without it we can’t read them. I put in an order for the correct crystal immediately and asked for fast shipping. If it gets here in we should be able to test it

## 3/21/23 Soldering
I finished soldering what I could on the PCB. Because we’re waiting for the crystal I decided not to connect the inductor coils since they are sensitive components.
 
## 3/23/23 Crystal
Got updates on the crystal that it won’t arrive until Monday which is the day of the demonstration. While this is disheartening if it arrives early I can try to go into the lab and solder it to the PCB, program the chip and test what works on the PCB.

## 3/24/23 Crystal Cont
Crystal delivery is delayed until Wednesday now. Because this is after our demonstration today I focused on preparing for the mock presentation on thursday. We quickly went over what we had working for the demonstration.

## 3/25/23
Worked on the presentation slides for thursday and final paper.

## 3/27/23
Mock presented and worked on feedback to improve the presentation as well as continued to work on the final paper. 

## 5/03/23 
Final Presentation and turned in final paper.s

