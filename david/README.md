# David's Lab Notebook
- The different pages make up different lab notebook entries
# Outline
1. Initial Project Idea Discussion
    - i.e., what are some inconveniences we're facing
    - discussing whether we want to do a pitch project or develop our own thing
2. Agree on Nich's proposal for the RFID lock
3. Initial Discussion with TA's
   - Determine the project needs something to differentiate it from othe products that exist on the market
4. Nich proposes wirelessly powering the lock with induction power
    - Move forward with this idea
5. Begin working on project proposal
    - Creating block diagram
    - Parts Research
    - Deciding on MLX90109 for solderable footprint
    - Deciding on STMF103 because it's cheap and solderable
    - Work out power requirements for microcontroller and rfid reader
6. Begin working on Design Document
    - For me, mostly small chip updates, and additional verfications, contributions based largely off of design documents from previous students
7. Team Contract
    - Collaborative effort, everyone seemed to agree
8. Design Review
    - Design Doc feedback, R&V table not filled out enough, needs more detail
9. Teamwork evaluation
10. Flash concerns, change chip?
    - Chip was changed
11. Recognize flaw in design, MLX90109 needs several peripherals to read NFC tag
12. Conversation with Melexis representative
13. No NFC reader has the antenna integrated, will need to purchase an antenna
14. End up switching to ST25R95-VM5DT
15. Concern regarding microcontroller flash memroy, switch to STM32F401RET6 which is similarly priced but with significantly more RAM and FLASH
16. Switch to ST25R3916-AQWT on account of clearer diagram.
17. Purchase Nucleo-401RE for rapid prototyping of the microcontroller
18. Research how to write code for the STM32 microcontroller
    - Arduino IDE, mbedOS w mbedStudio, STM32CubeIDE
19. Decide to use mbed Studio for clear documentation
20. Purchase sample servo motor to be able to do testing at home
21. Experiment with Servo libraries
22. Eventually end up using PWM code (500, 600 too little, 1500 pulses enough)
23. Purchase NUCLEO-NFC03A1 go back to ST25R95 since nucleo buitl on that
24. Research shows molex has a good antenna, purchase molex external antenna for simplicty of connection and thin form factor
25. attempt to program microcontroller to communicate with NFC expansion board over SPI
26. Do further research discover mbedOS has community libraries for NFC boards
27. Failure to make the mbed community libraries work, begin researching alternative libraries
28. Discover both the stm32duino librarie and the STM32 library for NFC, could not program Nucleo board from Arduino IDE so attempted to use STM32CubeIDE
29. Unable to use STM32CubeMX code generator but able to get a sample application running on Nucleo Board.
30. Discvoer PlatformIO!
31. Move project over to PlatformIO using the Arduino framework which allows usage of the Arduino library for NFC interface
    - Have now moved from mbed Studio to Arduino IDE to STM32CubeIDE to PlatformIO
32. First ported Servo code to the PlatformIO, able to utilize the Arduino servo library instead, which successfully locked and unlocked the door
33. Began implementing the sample application which was able to read the nfc tags and my credit card
34. Trimmed down sample application and was still able to read cards
35. Back to research, began researching memory storage solutions
36. Tried to store keys in ram but this failed because the system is supposed to reboot every time teh card is pulled away
37. Researched writing to flash memory, found arduino libraries for writing to stm32 flash like an eeprom
38. Able to integrate reading cards and unlocking doors
39. First attempt at reading card and writing to memory crashed program
40. Further attempts failed
41. Using khoih-prog's FlashStorage_STM32 library, able to read and write from memory 
42. Able to connect writing card to memory, reading card from memory after receiving card from scan and unlocking door (test was initially done on servo)
43. Demoed full functionality to Nikhil
44. Ran into memory collision issues, did not affect core demo functionality so ignored
45. Attempted to integrate the wireless power unit with the servo motor - unable to transmit enough current, need to demo the two subsystem separately
46. Demo feedback went well, need to cut text and have handoffs but otherwise pretty good.