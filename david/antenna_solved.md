# Antenna Tuning Circuit
- Another benefit that came out of finding the expansion board was a clearly defined circuit for receving antenna data. As can be seen below, the datasheet for the expansion board also provided circuitry for connecting and tuning an antenna which solved our problem of using an external antenna on our PCB design.
  - ![antenna circuit](antenna_circuit.png)
- Finding out how to develop the antenna tuning circuit allowed me to design the entire circuit for the door subsystem. The circuit includes control buttons, the microcontroller, NFC reader and servo motoro control. It can be seen below:
  - ![door circuit](circuit_1.png)
  - ![door circuit](circuit_2.png)
- This was another really big step in getting our project to a successful point I think! I was really glad to se that ST provided some circuitry for attena tuning