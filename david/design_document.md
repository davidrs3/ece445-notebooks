# Design Document
- Design Document ended up being mostly the same information on my end as the Project Proposal
- Some information was more fleshed out like the usage of the microcontroller or the interaction of the different components but overall very similar.
- In terms of style, language, and format; a lot of my contributions to our design document were based off of design documents from the examples provided on the course website.