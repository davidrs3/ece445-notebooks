# How do I code these things?
- After buying the nucleo board I realized I didn't know how to write code for the microcontroller. 
- I assumed that it would be different from the ATTiny we had used during the soldering assignment so I did some research and found several options for development environments for the Nucleo Board
  - Mbed Studio
  - Arduino IDE
  - STM32CubeIDE
- For our project, I decided to use Mbed Studio since it seemed to have the simplest interface and quickest project startup. I had also seen some forums complaining about the quality of the Arduino system so I wanted to stay away from that.