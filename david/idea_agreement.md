# Agreeing on an idea
- After Nich proposed the RFID lock we decided to move forward with it as it hit our focus areas:
  - It solved an inconvenience
  - It was solvable with technolgy
  - Feasible within the scope of a senior design project
- After Discussing the project with the TAs it was determined that our project would need something to differentiate it. 
- A couple of differentitating ideas were thrown about. 
  - We were considering for a while using amorphous solar cells to power the unit. Amorphous solar cells are unique because they require **signficantly** less surface area than traditional solar cells making them very efficient for small scale applications.
- After discussions with TAs and amongst the group however we decided to go with wireless power as it would be more consistent and would not rely on ambient conditions. The wireless power would be induction based