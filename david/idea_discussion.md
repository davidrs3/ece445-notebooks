# Initial Project Idea Discussion
- Decided not to do a pitch project
- We weren't really super enthusiastic about most of the ideas
- We wanted to maintain control over the project in case we really felt strongly about our idea
- Since we decided to go with our own project we needed to figure out what we were trying to solve and where to get our inspiration from
- We knew we weren't trying to solve any groundbreaking issues, suggested we start by trying to solve something small, focus on things that inconvenience users where technology could be a useful solution
- One idea was an automatic drain cleaner, that would sense a build up of hair and grind it up so that user's wouldn't need to use something like a "drain weasel" or Drain-O
  - Scrapped this idea because it didn't seem super feasible and also didn't feel like it really solved anything substantial
- Some other ideas focused on working with food or other stuff but none of them really stuck out to us
- Nich proposed doing a mountable RFID lock that would be accessible for renters.