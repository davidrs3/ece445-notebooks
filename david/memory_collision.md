# Memory Collision
- Upon doing some further testing of the flash memory, I determined that I was running into memory colliision issues.
- There were some infrequent cases where I would scan a card that should have been present in memory but the system would crash indefinitely and could not find it.
- On the other side of the coin there were cases where I would scan a card that was not in memory and still be able to verify it and unlock the door, even though the door should have remained locked.
- I am trying to debug this but the demo is coming up and I'm not sure I'll be able to resolve it in time, it also for the most part does not impair functionality but does mess up the privacy requirement that we have.