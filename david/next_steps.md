# Next Steps for the Project
- Were we to continue the project there are a few next steps we could reasonably take
1. Redesign the wireless power circuit to output more current (would require higher rated mosfets and capacitors)
2. Implement a small watch battery of some sort to provide continuous power to the microcontroller so it can store keys in ram and will not need to boot up from cold
3. Complete the PCB implementation so the entire project can be demoed on the board
4. Redesign the lock system so that it can easily slip over the door
# Stretch Goals
1. Add mobile phone NFC support so users would not need to receive a physical card
2. Get our receiver circuit qi certified so that systems like Samsung's *reverse wireless charging* could power it