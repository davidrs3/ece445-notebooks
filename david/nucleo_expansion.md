# NFC03A1
- I realized that I needed a way to test the NFC reader quickly so that when we were ready to put the unit on the PCB we would not need to go through testing on PCB directly which can be difficult to interface.
- Through some digging I discvoered that ST also produced Nucleo *expansion* boards which connected to their main nucleo boards with Arduino Morpho connectors
- I unfortunately found out that the available expansion board used the ST2525 chip so I had to double back and order that reader since that was ;supported by the expansion board I wanted to use
- What was nice about the expansion board also was that it had an antenna etched onto its PCB, making it one simply integrated unit
  - ![Nucleo-NFC03A1](nfc03a1.jpeg)