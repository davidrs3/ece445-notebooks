# Project Proposal
- We began working on the project proposal, for this I was in charge of two things, namely the block diagram and finding parts for the project. The block diagram can be seen below:
![block diagram](block_diagram.png)
- The block diagram breaks up the system into three logical pieces 
  - there is a section for the wireless power transmission, which consists of the wireless power transmitter and receiver
  - there is a section for the unlocking and locking of the door, which consists of the microcontroller, the rfid reader and the the servo motor that unlocks the door.
  - Lastly, there is a subssytem for user management where users can add or remove RFID keys.
- Outside of the block diagram was also responsible for researching ICs that would be used for the door subsystem
  - Searching for a microcontroller and rfid reader
    - Eventually decided on MLX90109 for the rfid reader because it had easily soldered feet compared to other components and the documentation was sensible. Here's a image of the chip:
      - ![MLX90109](mlx90109.jpeg)
    - Alongside the mlx chip i decided to go with an st microcontroller, the stm32f103 series which is cheap (~$6) with a fair amount of RAM
      - ![STM32f103c8t6](stm32f103c.png)
      -  