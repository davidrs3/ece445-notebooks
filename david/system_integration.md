# System Integration
- Finally, we are trying to integrate teh working door subsytem with the wireless power subsystem. 
- Nich has stepped up the wireless power subsystem with a 3.3V Li-ion battery to ensrue that it is able to put out the necessary 5V for the door system to operate
- We ran a couple of different trials but unfortunately, we figured out that the wireless power circuit, while able to put out the correct voltage is not able to put out enough current, we were tapping out below 1A and need 3A at peak current draw. 
- This is occurring really close to our final demo so we will need to demo the subsystems