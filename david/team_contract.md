# Team Contract
- Team Contract focused heavily on open and equal communication
- We made sure to emphasize people coming to the weekly TA meeting and being in touch with each other if we were working on things and werent' able to finish in time
- We defined team roles, I'm responsible for the NFC implementation and any accompanying software. Nich is taking the lead on the wirless power circuit design and Arely is handling all things related to the PCB
- Hopefully I can stick to the contract. A bit nervous that my current schedule will lead to situations where I might fall behind with design work.