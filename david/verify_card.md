# Verifying a card
- One of our requirements is that the application reads a card from memory. Right now, the system opens for any card. A way to store cards and then check them needs to be defined. 
- First thought is to use a vector or dictionary to store the cards, howeveer, this wouldn't work because that would store the cards in RAM and since the microcontroller is powered entirely from the wireless power unit it is frequently booted from cold with empty RAM.
- This means that memory must be written to Flash, the Arduino STM32 SDK contains a library called EEPROM that treats an ST chip's Flash memory like a smaller chip's EEPROM. 
  - Initially attempted to use this but the indexing was unclear so I looked for another method
- I found khoih-prog's FlashStorage_STM32 library in the platformIO reposityory and attempted to use that. I was able to successfully write a card to memory Alongside being able to write a card to memory I was also able to verify taht the card was in memory. 
- This meant that all of the pieces were in place, I was able to stitch all of the pieces together to read a card, verify the card, and unlock the door which meant that on an implementation side the door subsystem was complete!
- Here is a picture of the working circuit!
  - ![Complete BreadBoard](full_breadboard.png)