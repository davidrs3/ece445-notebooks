#include <Servo.h>
#include <Arduino.h>

#define DOOR_MAX_POS    180
#define DOOR_MIN_POS    0
#define DOOR_PIN        D3

void unlockDoor(void);
void lockDoor(void);