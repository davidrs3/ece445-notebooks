//include arduino libraries and util functions
#include "utils.h"

// include NFC libraries
// #include "nfc_utils.h"
#include "rfal_nfc.h"
#include "rfal_rfst25r95.h"
#include "ndef_class.h"

// define pins used by system
#define SPI_MOSI        D11
#define SPI_MISO        D12
#define SPI_SCK         D13
#define CS_PIN          D10
#define IRQ_IN_PIN      D8
#define IRQ_OUT_PIN     D2
#define INTERFACE_PIN   D9

// define read states
#define DEMO_ST_NOTINIT         0
#define DEMO_ST_START_DISCOVERY 1
#define DEMO_ST_DISCOVERY       2

// define read feature
#define NDEF_DEMO_READ  0U

// maximum amount of features (only using read, set to 1)
#define NDEF_DEMO_MAX_FEATURES  1U

#define NDEF_LED_BLINK_DURATION     250U
#define DEMO_ST_MANUFACTURER_ID     0x02U

typedef struct {
    char *tag_type;
    char *nfcUID;
} reader_response_t;

reader_response_t readNFCUID(void);
void initDevice(void);

