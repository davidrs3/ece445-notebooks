#pragma once
#include <Arduino.h>
#include <SPI.h>

#define LED_A_PIN       D7
#define LED_B_PIN       D6
#define LED_F_PIN       D5
#define LED_V_PIN       D4
#define MAX_HEX_STR         4
#define MAX_HEX_STR_LENGTH  128

void ledsOn(void);
void ledsOff(void);
char *hex2str(unsigned char *data, size_t dataLen);