#include "door.h"

static Servo servo;

void unlockDoor(void)
{
    servo.attach(DOOR_PIN);
    for(int pos = DOOR_MAX_POS; pos >= DOOR_MIN_POS; pos--)
    {
        servo.write(pos);
    }
}

void lockDoor(void)
{
    servo.attach(DOOR_PIN);
    for(int pos = DOOR_MIN_POS; pos >= DOOR_MAX_POS; pos++)
    {
        servo.write(pos);
    }
}