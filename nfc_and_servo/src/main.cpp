#include "nfc_read.h"
#include "door.h"
#include <FlashStorage_STM32.h>

#define PASSIVE_ISO_DEP_LEN 14
#define MAX_NUM_KEYS 10
#define NUM_KEYS_INDEX 200

static bool verifyUID(const char *uid);

static void addKeyToMemory(const char* uid);

static void interruptCallback(void);

/**
 * numkeys becomes a global variable held in ram rather than something written to flash memory
 * this simplifies the operation of adding keys as fewer writes to flash are required and indexing is simplified
 * BENEFITS: fewer writes to flash memory(increased longevity), simplified memory indexing
 * COSTS: more refresh cycles necessary for RAM, decrease the life of the ram, also increased 
 * dependency on global variables reduces encapsulation somewhat
*/
static int numkeys = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200); // setting baudrate
  delay(1000);


  pinMode(LED_A_PIN, OUTPUT);
  pinMode(LED_B_PIN, OUTPUT);
  pinMode(LED_F_PIN, OUTPUT);
  pinMode(LED_V_PIN, OUTPUT);
  pinMode(USER_BTN, INPUT);

  attachInterrupt(digitalPinToInterrupt(USER_BTN), interruptCallback, RISING);

  // /* check state of the push button when not pressed */
  Serial.println("Welcome to X-NUCLEO-NFC03A1");
  Serial.print(numkeys);
  initDevice();
  delay(1000);
  reader_response_t readerResponse = readNFCUID();
  delay(1000);
  if (readerResponse.nfcUID && readerResponse.tag_type)
  {
    Serial.println("received uid");
    delay(1000);
    Serial.write(readerResponse.nfcUID);
    Serial.write(readerResponse.tag_type);
    delay(1000);
    addKeyToMemory(readerResponse.nfcUID);
  }
  else
  {
    Serial.print("no UID received");
  }

  // Serial.print("Going to unlock the door");
  // Serial.print("\r\n");
  // delay(2000);
  // unlockDoor();
  // Serial.print("Unlocked the Door");
  // Serial.print("\r\n");
  // delay(3000);
  // Serial.print("Locking Door");
  // Serial.print("\r\n");
  // lockDoor();
  // Serial.print("Locked Door, ending execution");
  // Serial.print("\r\n");
}

void loop() {
  // put your main code here, to run repeatedly:
  reader_response_t readerResponse = readNFCUID();

  if(readerResponse.nfcUID && readerResponse.tag_type)
  {
    if (verifyUID(readerResponse.nfcUID) == true)
    {
      Serial.print("valid uid");
      Serial.print("\r\n");
      Serial.print("running unlock door");
      Serial.print("\r\n");
      unlockDoor();
      delay(1000);
      Serial.print("unlocked door");
      Serial.print("\r\n");
      delay(6000);
      Serial.print("locking door");
      Serial.print("\r\n");
      lockDoor();
      delay(1000);
      Serial.print("locked door");
      Serial.print("\r\n");
    }
    else 
    {
      Serial.print("uid not found");
      Serial.print("\r\n");
    }
  }
  else 
  {
    Serial.print("no uid received");
    Serial.print("\r\n");
  }
}

static bool verifyUID(const char *uid)
{

  int increaseIndex = PASSIVE_ISO_DEP_LEN * sizeof(char);
  for (int i = 0; i < 10 * increaseIndex; i+= increaseIndex)
  {
    char* eeprom_uid;
    Serial.print("Reading from memory");
    EEPROM.get(i, eeprom_uid);
    delay(1000);
    Serial.write(eeprom_uid);
    if (strncmp(uid, eeprom_uid, strlen(uid)) == 0)
    {
      return true;
    }
    else
    {
      continue;
    }
  }
  return false;
}

static void addKeyToMemory(const char* uid)
{
  delay(500);
  Serial.print(numkeys);
  if(numkeys < 10){
    EEPROM.put(numkeys * PASSIVE_ISO_DEP_LEN * sizeof(char), uid);
    delay(500);
    numkeys++;
    delay(500);
    Serial.print("Successfully added keys");
  }
  else
  {
    Serial.print("Could not add key");
  }
}

static void interruptCallback(void) 
{
  Serial.write("add a new key to memory\r\n");
  reader_response_t readerResponse = readNFCUID();
  if(readerResponse.nfcUID && readerResponse.tag_type)
  {
    Serial.write("writing key to memory\r\n");
    addKeyToMemory(readerResponse.nfcUID);
    Serial.write("key has been written to memory, returning to normal flow \r\n");
  }
  else 
  {
    Serial.write("new key could not be read\r\n");
  }
}
