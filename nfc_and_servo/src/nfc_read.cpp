#include "nfc_read.h"

static uint8_t state = DEMO_ST_NOTINIT;
static uint8_t ndefDemoFeature = NDEF_DEMO_READ;
static uint8_t ndefDemoPrevFeature = 0xFF;
static uint8_t NFCID3[] = {0x01, 0xFE, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A};
static uint8_t GB[] = {0x46, 0x66, 0x6d, 0x01, 0x01, 0x11, 0x02, 0x02, 0x07, 0x80, 0x03, 0x02, 0x00, 0x03, 0x04, 0x01, 0x32, 0x07, 0x01, 0x03};
static rfalNfcDiscoverParam discParam;
static SPIClass dev_spi(SPI_MOSI, SPI_MISO, SPI_SCK);
static RfalRfST25R95Class rfst25r95(&dev_spi, CS_PIN, IRQ_IN_PIN, IRQ_OUT_PIN, INTERFACE_PIN);
static RfalNfcClass rfal_nfc(&rfst25r95);
static NdefClass ndef(&rfal_nfc);
static uint32_t timer;
static uint32_t timerLed;
static bool ledOn;
static const uint8_t *ndefDemoFeatureDescription[] = {
    (uint8_t *)"1. Tap a tag to read its content"};

void initDevice(void)
{
    dev_spi.begin();

    if (rfal_nfc.rfalNfcInitialize() == ERR_NONE)
    {
        discParam.compMode = RFAL_COMPLIANCE_MODE_NFC;
        discParam.devLimit = 1U;
        discParam.nfcfBR = RFAL_BR_212;
        discParam.ap2pBR = RFAL_BR_424;

        ST_MEMCPY(&discParam.nfcid3, NFCID3, sizeof(NFCID3));
        ST_MEMCPY(&discParam.GB, GB, sizeof(GB));
        discParam.GBLen = sizeof(GB);

        discParam.notifyCb = NULL;
        discParam.totalDuration = 100U;
        discParam.wakeupEnabled = false;
        discParam.wakeupConfigDefault = true;
        discParam.techs2Find = (RFAL_NFC_POLL_TECH_A | RFAL_NFC_POLL_TECH_B | RFAL_NFC_POLL_TECH_F | RFAL_NFC_POLL_TECH_V | RFAL_NFC_POLL_TECH_ST25TB);

        state = DEMO_ST_START_DISCOVERY;
    }
}

reader_response_t readNFCUID(void)
{
    while (true)
    {
        static rfalNfcDevice *nfcDevice;

        rfalNfcaSensRes sensRes;
        rfalNfcaSelRes selRes;

        rfalNfcbSensbRes sensbRes;
        uint8_t sensbResLen;

        uint8_t devCnt = 0;
        rfalFeliCaPollRes cardList[1];
        uint8_t collisions = 0U;
        rfalNfcfSensfRes *sensfRes;

        rfalNfcvInventoryRes invRes;
        uint8_t rcvdLen;
        char *uidData = nullptr;

        rfal_nfc.rfalNfcWorker();

        if (ndefDemoFeature != ndefDemoPrevFeature)
        {
            ndefDemoPrevFeature = ndefDemoFeature;
            Serial.print((char *)ndefDemoFeatureDescription[ndefDemoFeature]);
            Serial.print("\r\n");
        }
        switch (state)
        {
        case DEMO_ST_START_DISCOVERY:
            ledsOff();

            rfal_nfc.rfalNfcDeactivate(false);
            rfal_nfc.rfalNfcDiscover(&discParam);

            state = DEMO_ST_DISCOVERY;
            break;

        case DEMO_ST_DISCOVERY:
            if (rfalNfcIsDevActivated(rfal_nfc.rfalNfcGetState()))
            {
                rfal_nfc.rfalNfcGetActiveDevice(&nfcDevice);

                ledsOff();

                delay(50);
                ndefDemoPrevFeature = 0xFF; /* Force Display of prompt */
                reader_response_t readerResponse = {nullptr, nullptr};
                switch (nfcDevice->type)
                {
                case RFAL_NFC_LISTEN_TYPE_NFCA:
                    digitalWrite(LED_A_PIN, HIGH);
                    switch (nfcDevice->dev.nfca.type)
                    {
                    case RFAL_NFCA_T1T:
                    {
                        Serial.print("ISO14443A/Topaz (NFC-A T1T) TAG found. UID: ");
                        readerResponse.tag_type = "ISO1443A/Topaz (NFC-A T1T)";
                        Serial.print(hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen));
                        Serial.print("\r\n");
                        rfal_nfc.rfalNfcaPollerSleep();
                        break;
                    }
                    
                    case RFAL_NFCA_T4T:
                        Serial.print("NFCA PASSIVE ISO-DEP device found. UID: ");
                        readerResponse.tag_type = "NFCA PASSIVE ISO-DEP";
                        Serial.print(hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen));
                        Serial.print("\r\n");
                        // demoNdef(nfcDevice);
                        rfal_nfc.rfalIsoDepDeselect();
                        break;

                    case RFAL_NFCA_T4T_NFCDEP:
                    case RFAL_NFCA_NFCDEP:
                    {
                        Serial.print("NFCA Passive P2P device found. NFCID: ");
                        char tagType[] = "NFCA Passive P2P";
                        // readerResponse.tag_type = "NFCA Passive P2P";
                        readerResponse.tag_type = tagType;
                        Serial.print(hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen));
                        Serial.print("\r\n");
                        // demoP2P();
                        break;
                    }

                    default:
                        Serial.print("ISO14443A/NFC-A card found. UID: ");
                        readerResponse.tag_type = "ISO1443A/NFC-A";
                        Serial.print(hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen));
                        Serial.print("\r\n");
                        // demoNdef(nfcDevice);
                        rfal_nfc.rfalNfcaPollerSleep();
                        break;
                    }
                    Serial.print("Operation completed\r\nTag can be removed from the field\r\n");
                    uidData = hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen);
                    rfal_nfc.rfalNfcDeactivate(true);
                    // return uidData;
                    readerResponse.nfcUID = uidData;
                    return readerResponse;
                    rfal_nfc.rfalNfcaPollerInitialize();
                    while (rfal_nfc.rfalNfcaPollerCheckPresence(RFAL_14443A_SHORTFRAME_CMD_WUPA, &sensRes) == ERR_NONE)
                    {
                        if (((nfcDevice->dev.nfca.type == RFAL_NFCA_T1T) &&
                             (!rfalNfcaIsSensResT1T(&sensRes))) ||
                            ((nfcDevice->dev.nfca.type != RFAL_NFCA_T1T) &&
                             (rfal_nfc.rfalNfcaPollerSelect(nfcDevice->dev.nfca.nfcId1,
                                                            nfcDevice->dev.nfca.nfcId1Len, &selRes) != ERR_NONE)))
                        {
                            break;
                        }
                        rfal_nfc.rfalNfcaPollerSleep();
                        delay(130);
                    }
                    break;

                case RFAL_NFC_LISTEN_TYPE_NFCB:

                    Serial.print("ISO14443B/NFC-B card found. UID: ");
                    readerResponse.tag_type = "ISO14443B/NFC-B";
                    Serial.print(hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen));
                    Serial.print("\r\n");
                    // uidData = hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen);
                    digitalWrite(LED_B_PIN, HIGH);
                    // rfal_nfc.rfalNfcDeactivate(true);
                    // return uidData;

                    if (rfalNfcbIsIsoDepSupported(&nfcDevice->dev.nfcb))
                    {
                        // demoNdef(nfcDevice);
                        rfal_nfc.rfalIsoDepDeselect();
                    }
                    else
                    {
                        rfal_nfc.rfalNfcbPollerSleep(nfcDevice->dev.nfcb.sensbRes.nfcid0);
                    }
                    /* Loop until tag is removed from the field */
                    Serial.print("Operation completed\r\nTag can be removed from the field\r\n");
                    uidData = hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen);
                    rfal_nfc.rfalNfcDeactivate(true);
                    // return uidData;
                    readerResponse.nfcUID = uidData;
                    return readerResponse;
                    rfal_nfc.rfalNfcbPollerInitialize();
                    while (rfal_nfc.rfalNfcbPollerCheckPresence(RFAL_NFCB_SENS_CMD_ALLB_REQ, RFAL_NFCB_SLOT_NUM_1, &sensbRes, &sensbResLen) == ERR_NONE)
                    {
                        if (ST_BYTECMP(sensbRes.nfcid0, nfcDevice->dev.nfcb.sensbRes.nfcid0, RFAL_NFCB_NFCID0_LEN) != 0)
                        {
                            break;
                        }
                        rfal_nfc.rfalNfcbPollerSleep(nfcDevice->dev.nfcb.sensbRes.nfcid0);
                        delay(130);
                    }
                    break;

                case RFAL_NFC_LISTEN_TYPE_NFCF:

                    if (rfalNfcfIsNfcDepSupported(&nfcDevice->dev.nfcf))
                    {
                        Serial.print("NFCF Passive P2P device found. NFCID: ");
                        readerResponse.tag_type = "NFCF Passive P2P device";
                        Serial.print(hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen));
                        Serial.print("\r\n");
                        // demoP2P();
                    }
                    else
                    {
                        Serial.print("Felica/NFC-F card found. UID: ");
                        Serial.print(hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen));
                        Serial.print("\r\n");
                        // demoNdef(nfcDevice);
                    }

                    digitalWrite(LED_F_PIN, HIGH);
                    /* Loop until tag is removed from the field */
                    Serial.print("Operation completed\r\nTag can be removed from the field\r\n");
                    uidData = hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen);
                    rfal_nfc.rfalNfcDeactivate(true);
                    // return uidData;
                    readerResponse.nfcUID = uidData;
                    return readerResponse;
                    devCnt = 1;
                    rfal_nfc.rfalNfcfPollerInitialize(RFAL_BR_212);
                    while (rfal_nfc.rfalNfcfPollerPoll(RFAL_FELICA_1_SLOT, RFAL_NFCF_SYSTEMCODE, RFAL_FELICA_POLL_RC_NO_REQUEST, cardList, &devCnt, &collisions) == ERR_NONE)
                    {
                        /* Skip the length field byte */
                        sensfRes = (rfalNfcfSensfRes *)&((uint8_t *)cardList)[1];
                        if (ST_BYTECMP(sensfRes->NFCID2, nfcDevice->dev.nfcf.sensfRes.NFCID2, RFAL_NFCF_NFCID2_LEN) != 0)
                        {
                            break;
                        }
                        delay(130);
                    }
                    break;

                case RFAL_NFC_LISTEN_TYPE_NFCV:
                {
                    uint8_t devUID[RFAL_NFCV_UID_LEN];

                    ST_MEMCPY(devUID, nfcDevice->nfcid, nfcDevice->nfcidLen); /* Copy the UID into local var */
                    REVERSE_BYTES(devUID, RFAL_NFCV_UID_LEN);                 /* Reverse the UID for display purposes */
                    Serial.print("ISO15693/NFC-V card found. UID: ");
                    readerResponse.tag_type = "ISO15693/NFC-V";
                    Serial.print(hex2str(devUID, RFAL_NFCV_UID_LEN));
                    Serial.print("\r\n");
                    uidData = hex2str(devUID, RFAL_NFCV_UID_LEN);

                    digitalWrite(LED_V_PIN, HIGH);


                    // demoNdef(nfcDevice);

                    /* Loop until tag is removed from the field */
                    Serial.print("Operation completed\r\nTag can be removed from the field\r\n");
                    // uidData = nfcDevice->nfcid;
                    rfal_nfc.rfalNfcDeactivate(true);
                    readerResponse.nfcUID = uidData;
                    // return uidData;
                    return readerResponse;
                    rfal_nfc.rfalNfcvPollerInitialize();
                    while (rfal_nfc.rfalNfcvPollerInventory(RFAL_NFCV_NUM_SLOTS_1, RFAL_NFCV_UID_LEN * 8U, nfcDevice->dev.nfcv.InvRes.UID, &invRes, (uint16_t *)&rcvdLen) == ERR_NONE)
                    {
                        delay(130);
                    }
                }
                break;

                case RFAL_NFC_LISTEN_TYPE_ST25TB:

                    Serial.print("ST25TB card found. UID: ");
                    readerResponse.tag_type = "ST25TB";
                    Serial.print(hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen));
                    Serial.print("\r\n");
                    readerResponse.nfcUID = hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen);
                    return readerResponse;
                    digitalWrite(LED_B_PIN, HIGH);
                    break;

                case RFAL_NFC_LISTEN_TYPE_AP2P:

                    Serial.print("NFC Active P2P device found. NFCID3: ");
                    readerResponse.tag_type = "NFC Active P2P";
                    Serial.print(hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen));
                    Serial.print("\r\n");
                    readerResponse.nfcUID = hex2str(nfcDevice->nfcid, nfcDevice->nfcidLen);
                    return readerResponse;
                    // demoP2P();
                    break;

                default:
                    break;
                }

                rfal_nfc.rfalNfcDeactivate(true);
                delay(500);
                state = DEMO_ST_START_DISCOVERY;
            }
            break;

        case DEMO_ST_NOTINIT:
        default:
            break;
        }
    }
}