#include "utils.h"
#include "nfc_utils.h"

char hexStr[MAX_HEX_STR][MAX_HEX_STR_LENGTH];
uint8_t hexStrIdx = 0;

void ledsOn(void)
{
  digitalWrite(LED_A_PIN, HIGH);
  digitalWrite(LED_B_PIN, HIGH);
  digitalWrite(LED_F_PIN, HIGH);
  digitalWrite(LED_V_PIN, HIGH);
}

void ledsOff(void)
{
  digitalWrite(LED_A_PIN, LOW);
  digitalWrite(LED_B_PIN, LOW);
  digitalWrite(LED_F_PIN, LOW);
  digitalWrite(LED_V_PIN, LOW);    
}

char *hex2str(unsigned char *data, size_t dataLen)
{
  unsigned char *pin = data;
  const char *hex = "0123456789ABCDEF";
  char *pout = hexStr[hexStrIdx];
  uint8_t i = 0;
  uint8_t idx = hexStrIdx;
  size_t len;

  if (dataLen == 0) 
  {
    pout[0] = 0;
  }
  else
  {
    /* Trim data taht doesn't fit in buffer */
    len = MIN(dataLen, (MAX_HEX_STR_LENGTH / 2));

    for(; i < (len - 1); ++i)
    {
      *pout++ = hex[(*pin >> 4) & 0xF];
      *pout++ = hex[(*pin++) & 0xF];
      *pout = 0;
    }
    *pout++ = hex[(*pin >> 4) & 0xF];
    *pout++ = hex[(*pin) & 0xF];
    *pout = 0;
  }

  hexStrIdx++;
  hexStrIdx %= MAX_HEX_STR;

  return hexStr[idx];

}