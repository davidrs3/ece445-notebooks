ECE 445 Lab Notes

=======================================.

Date: 1/30/2023

Topics Covered: Discussion of project with head T.A.

Notes: 

Original idea started as just an RFID door lock, this idea was not approved due to the fact so many already exist but our 
idea to create one that works with existing deadbolt / door locks was unique so was left open by the professor.
Further talks with Jason lead to ideas on making this project more complex. One idea included induction power.


======================================= (End of date notes)

=======================================.

Date: 1/31/2023

Topics Covered: Discussion of project with professors after class and TA

Notes: 

After developing the induction power idea a little more, I discussed this with another lab TA during the day. He
said he would take a look at our board post and try and get our project approved. I also discussed how to avoid
any passive power draw from batteries when a circuit is running with professor Mironenko and Jason after class
but we further discussed induction power being the only source here and not to have any battery power in the circuit.

No batteries would later become a high level requirement and issue for our project.


=======================================.

=======================================.

Date: 2/13/2023

Topics Covered: Initial conversation with machine shop

Notes: 

We were late to the machine shop in discussing our project with them since they wanted information and feedback on 2/10.
Discussed our project and how we wanted to use something to rotate the door, initial idea was a linear actuator but they
showed us a previous example of how they used a servo motor mounted to the deadbolt via a custom base plate. This wasn't
exactly what I was looking for but since I was late and they had other projects this would be good enough for proof of 
concept.

They had a 3.5 lb-in motor available for purchase at the ece shop but they told us to source one with more torque
to make sure it could rotate any and all deadbolts. This was difficult to find a servo that could run on low
voltage, fit on a deadbolt lock and had high torque. We ended up finding the Pololu FS5115M which can run at 5V.
This motor also has a torque rating of 13.5 lb-in so much better suited for our needs.


=======================================.

=======================================.

Date: 2/15/2023

Topics Covered: Circuit component Ideas

Notes: 

Lots of notes taken today on possible ideas for both induction charing and the circuit design itself.

Wireless power transmitter found [here](https://www.digikey.com/en/products/detail/tdk-corporation/WRM483265-10F5-12V-G/10484695?utm_adgroup=Wireless%20Charging%20Coils&utm_source=google&utm_medium=cpc&utm_campaign=Shopping_Product_Inductors%2C%20Coils%2C%20Chokes&utm_term=&utm_content=Wireless%20Charging%20Coils&gclid=EAIaIQobChMI6L6y5_7v_AIV8oJbCh120giXEAQYAiABEgJQlfD_BwE) is attached to phone case or in credit card sized wallet insert next to RFID tag

Wireless power receiver found [here](https://www.digikey.com/en/products/detail/vishay-dale/IWAS4832AEEB120KF1/10223651?utm_adgroup=Inductors&utm_source=google&utm_medium=cpc&utm_campaign=Shopping_Supplier_Vishay&utm_term=&utm_content=Inductors&gclid=EAIaIQobChMIgLLB2v7v_AIVKcmUCR1wXQ7uEAQYASABEgIWafD_BwE) is set inside door next to rfid scanner

If RFID scanner powered by a battery inside the door accepts rfid tag in wallet or phone attachment (same attachment that carried power transmitter) then it draws power from the power transmitter and uses linear actuator ([more details on linear actuator](https://www.u-buy.com.ng/product/CLCT30Y-mini-electric-linear-actuator-stroke-3-force-4-5-lbs-12v-high-speed-0-6-sec-weight-0-2kg-ideal-for-i)) to push door deadbolt.

^^^Possible amendment to above – The battery pack powers the entire board via wireless power, so RFID scanner, microprocessor etc all get powered from wireless transmitter and if RFID accepts then it turns on linear actuator. (Issue here is now we cannot add any passive functionality because it has no power without the wireless battery pack transmitting power applied to it.


We need a battery pack to store power to transmit, if it is rechargeable that would be even better
Some rechargeable battery packs found [here](https://www.dnkpower.com/12v-rechargeable-lithium-ion-battery/) and a flat one [here](https://www.dnkpower.com/products/12v-rechargeable-lithium-pack-dnk-ltb3s1pc18h/)

For microcontroller/processor there are a bunch of recommendations [here](https://blog.snapeda.com/2016/08/04/the-top-10-microcontrollers-for-pcb-designers/), Im genuinely not sure how to pick one out I think [this](https://www.google.com/url?q=https://www.snapeda.com/parts/PIC16F886-I/SO/Microchip/view-part/&sa=D&source=docs&ust=1683231683226179&usg=AOvVaw3bP8Ui0j-oTi9cww0FDTh9) one will do. David later picked out the microcontroller used in circuit.
Potential Microcontrollers
https://www.snapeda.com/parts/STM32F103C8T6/STMicroelectronics/view-part/
Power consumption: 2.0-3.6V


What about instead of wireless power transfer we use amorphous cells
Possible options:
https://www.digikey.com/en/products/detail/panasonic-bsg/AM-1801CA/869-1003-ND/2165188
https://www.digikey.com/en/products/detail/panasonic-bsg/AM-1417CA-DGK-E/2165185
RFID Scanners
https://www.digikey.com/en/products/detail/timepilot-corporation/RS-H3-05-M12/17000601
https://www.digikey.com/en/products/detail/timepilot-corporation/RS-H0-05-M12/17000637







=======================================.

=======================================.

Date: 2/16/2023

Topics Covered: Induction circuit ideas

Notes: 

We were told we had to create the circuit connected to the induction coils and operate that ourselves.
We would need to create it, test it and print it on a pcb. The following are the ideas I came up with.

For inductance we need AC aka an inverter but instead we can find a cool little circuit that does so in [this video](https://www.youtube.com/watch?v=eNZ8KPHYDvg&ab_channel=Electronoobs). Output voltage from one coil to another depends on turn ration (turns in transmitter to turns in receiver)
Vp/Vs = Np/Ns
Meaning if we want higher secondary voltage (aka voltage in receiver) 
Vs = (Vp *Ns) / Np
we need to increase the number of turns in the secondary (receiver) coil

Main components are coils and the mosfet highlighted. Since we are making our own circuit we cannot just buy a transmitter but instead can buy two receivers and create corresponding circuits to make one transmit power and one receive power. Mosfet we can use [this one](https://www.digikey.com/en/products/detail/infineon-technologies/IRFZ44NPBF/811772) rated for 20v which is a bit overkill but useful incase we need to up our voltage/power depending on if we need a stronger motor etc. it also has some nice sexy legs making it easier to solder (may not be as pretty or lay as flat on pcb)



=======================================.

=======================================.

Date: Week of 2/20/2023

Topics Covered: Design Doc Prep

Notes: 

This week was spent entirely on creating documentation for the cirucit ideas we had so far. What motor we had, the circuit I had come up with using examples from the youtube video previously linked. This included explaining the circuit, what the capacitors did and how they helped the circuit voltage drop more smoothly when fed an AC voltage in the induced emf. I also created nicer looking
graphics for the visual aids since the ms paint hand drawn ones were not cutting it. Figured out how long servo motor lifespans were, how many uses we could get etc. 


=======================================.

=======================================.

Date: 2/28/2023

Topics Covered:Peak current draw from MicroController, RFID Reader, and Servo Motor

Notes: 

Starting by looking at Servo
Servo motor we are using is the Pololu FS5115M
Data sheet here (https://www.pololu.com/product/3426/resources)
Looking at datasheet, operating at 4.8 V which is what we will be operating at there is a passive current draw of 5mA.
When running it pulls a current of 160 mA. However, the stall current is 1200 mA (1.2A).
This is peak amperage and motors should have this amperage and a little over just in case.
This peak amperage is usually hit upon start-up or when it starts to stall when turning a door lock/recieves resistance.
Since we need to account for this we need to be able to supply 1.2A at 5 V. This has both an idle, peak and operating current.
Pidle = 5mA*4.8V = 24 mW .  Ppeak = 1.2A * 4.8 V = 5.76 W. Poperating = 160 mA * 4.8 V = 768 mW.

Next we look at MicroController
We are using the STM32F103C8T6 microcontroller
Data sheet here (https://www.st.com/resource/en/datasheet/stm32f103cb.pdf)
Absolute maximum ratings for voltage is 4.0 Volts across the microcontroller and an operating range of 2 to 3.6 V.
We will likely keep an operating voltage of around 3.0 V. Maximum current into microcontroller is 150 mA.
Assuming we operate at maximum current to give us extra breathing room in case we do operate there P=IV = 3.0V * 150mA = 450 mW.

Finally looking at RFID tag reader datasheet
Data sheet here (https://www.digikey.com/en/products/detail/melexis-technologies-nv/MLX90109EDC-AAA-000-RE/1647934)
Operating voltage between 3.1 and 5.5 V. Likely keep operating voltage at 4.0 volts. The reading unit and antennae unit use 1.8 and 
2.8 mA respectively for a total of 4.6 mA for the RFID tag reader and antannae. 
P=IV for unit and antennae = 4.6mA*4.0V = 18.4 mW.

Additional components: Our circuit will have LED's as well for visual feedback to the user. 
No specific research has been done on LED types but a guess of around 8 LED's will be used for our unit. 
LED's draw an average of around 20 mA on the high end at a voltage of around 2.0 volts on average. 
Using 8LEDs * 20 mA per LED = 160 mA total. P=IV at 2 volts is 320 mW total for LEDs.

Putting this all together for an idle, peak and operating power draw depending on motor function.
Pidle = 24 mW + 450 mW+ 18.4 mW +320 mW = 812.4 mW = .8124 W
Iidle = 5 mA + 4.6 mA + 160 mA + 150 mA = 319.6 mA = .3196 A

Poperating = 768 mW + 450 mW+ 18.4 mW +320 mW = 1574.4 mW = 1.5744 W
Ioperating = 160 mA + 4.6 mA + 160 mA + 150 mA = 474.6 mA = .4745 A

Ppeak = 5.76 W + 450 mW+ 18.4 mW +320 mW = 6548.4 mW = 6.5484 W
Ipeak = 1200 mA + 4.6 mA + 160 mA + 150 mA = 1514.6 mA = 1.5146 A

Peak power and current needs to be our main focus on determining what our circuit can handle. 
We need to be able to reach our max power requirements. 

_______________________________________ (Different topic covered but still same date)
Topics Covered:Loss due to induction charging

Notes: 

Our induction coils are similar in size to those used in wireless/induction charging applications used for phones. 
Looking at some tests done on these phone wireless induction chargers the loss ranges from anywhere between 30% - 50% of total 
power between transmitter and reciever circuits for induction charging.
Sources:
(https://www.techspot.com/news/86271-wireless-charging-has-efficiency-issue.html), (https://www.infoworld.com/article/2616229/the-fallacy-of-wireless-power.html?page=3)

Looking at my previous notes on power that needs to be delivered to the operating circuit we have a peak of 6.5484 W at 1.5146 A. 
Meaning our transmitter coil needs to be given 30-50% more power than what is needed at the reciever. 6.5484 /.7 and 
6.5484/.5 gives 9.3548 to  13.0968 W respectively for our peak power. This is a lot of power requirement most of which 
is due to the peak/stall current from the servo. Likely our circuit will not be providing this power constantly but needs to be able 
to handle that level of current and power draw so it doesn't catch on fire or burn out components when operating at peak conditions.



======================================= (End of date notes)


Date: 3/1/2023

Topics Covered:Need to find new inductor coil and N type mosfet

Notes: 

After doing some power consumption and needs analysis yesterday, I took a look at the datasheet for the
inductance coil we had picked out. I saw it was rated for only 5W power meaning not enough for the peak
power that would be needed for the circuit to operate. I Found a new coil at link below
(https://www.digikey.com/en/products/detail/w%C3%BCrth-elektronik/760308101214/5725359)
This coil is rated for 20 W of consumption which should be enough for our purposes.
I also created an LC oscillator simulating the coil and circuit found at link below
[link](https://www.falstad.com/circuit/circuitjs.html?ctz=CQAgjCAMB0l3BWEBOaB2AHAZmcjAWBLANgwCYwsRjIQl8qEBTAWjDACgBzELBMkPji9+grANqQOAJRECwZDHOr4oaoSCWSo0BBwDGgjLTDIB+Y8tploZVmk064nAGbKyac2GIgPA8Y4wkGQGvgg+YGg+ZOG+ZKq0+OjwKamQYKzI4MTQFopgGMTE+PjFErDpoab+kKrVvKRqGSxZGLalhciQGGgkyMUJFZwATuCK4FFjSjE+tAjwHKPF4CUqYbPgC94CyzNrWCS+IAAmTC4AhgCuADYALizXTMfgapKwnIb1kRFmvhommU07VIxC6PSwaDQkGQg2cHAA7msFEplmBVlJEcsDj4sY0MftGvUSFoEeBfsTlBT8Xx-I0aYJhNTRH4pr5PFBSci2fJxnt8Vzvqy0QlOb9BV90aS9oLpZLEfhvOtBIqyPEOYiBasJSLEfSuXrfviFdEMEpjQ0SbrRBTzVTSRZaPSHRb1UZHY1bXipbFhWJ5HLldE1ebBUbFYL8P0JrN7QFfZGIgG9eMQ+MwxEU+GaBzrtRuitVAmC68Ks4BCwYGRIMQ0AgulWyMQED00BApLmi4KaEpQ054AoQBXoKYaAgFYozPE8CLRgbzJZ6iZScmUXU06L-apZTqlarCyq1fzfvETOTaq7u8XL6H7VnaNfJQB7Xh1dTQrLsaBYAjBA6eBBongH7Ds2yCQpQtYYN4GDIFQ1gvrwHBAA)

Below, you can find the initial circuit diagram that this lc oscillator was based off of

![](Transmittercircuit.JPG)

Figure 1.1 is the initial circuit diagram for the LC oscillator

This unit is 2-3x the price of the previous which makes sense since it is a higher rated component
that can transfer more power. This will be used on both the transmitter and reciever side.
The mosfet we had picked out also had a gate to source voltage of 20 V which is a bit high for
our purposes so I picked out a smaller 8 V drain to source mosfet found at link below.
[link](https://www.digikey.com/en/products/detail/vishay-siliconix/SI1050X-T1-GE3/2441547)
Data sheet here (https://www.vishay.com/docs/73896/si1050x.pdf)
We should order these parts asap since we need to begin building and troubleshooting our circuit.


=======================================.

=======================================.

Date: 3/3/2023

Topics Covered:Updating proposal and servo motor order

Notes:

Updated project proposal/proposal resubmission is due today. I went through and added in updates on both new parts 
and voltages in subsystem requirements as well as toelrance analysis on power requirements for the circuit since they went
hand in hand. Also final revisions for machine shop work are due soon (3/10), I was late on initial talks with machine shop 
so it would be nice to have this done on time. The work is minimal, they just need to mount the motor on a deadbolt baseplate.
However, if there is difficulty with this it would be better to notice and fix it sooner rather than later.

I ordered servo motor today so hopefully it will be in by next week to get it mounted. I also ordered mosfet, and induction coils
yesterday (3/2) so I will be able to work on circuit the week of (3/6) for power transmission. 




=======================================.

=======================================.

Date: 3/28/2023

Topics Covered: Motor mounted to deadbolt and breadboarded circuit

Notes:

One of the missing components I needed to complete my breadboarding was a mosfet, the one we had previously ordered for our board
had footprints that were too small to solder. Since they are too small to solder, we are unable to even attach wires to the feet
for purposes of breadboarding.

I picked one up a couple days ago at the ECE parts store (360N06L)
While doing so I also dropped off our servo motor at the machine shop. Today I was notified that the servo motor was mounted.
I have not picked it up yet but we need to run tests on it to make sure that the current draw for peak and operating current are 
the same or similar to the values given in the datasheet for the motor.

Scraping away some of this coating and getting a connection in the center of the inductor is called tapping.
Since we are limited in our purchasing power I do not want to mess up an inductor by scraping away protective
coating just yet.

Today I completed breadboarding the power transmitter circuit. One issue I ran into is the wire connecting to the center of the
inductor. Since the inductors we ordered come with a insulated coating necessary to make the inductors work, I had to just use 
both inductors in parallel to act line one big inductor to be able to make that connection from + to the center of the inductor as shown 
in the falstad simulation/ circuit diagram. I believe the circuit to be working properly but I am having issues with getting the 
oscilliscope to read properly. You can see an example of the wire tapping I did in the image below, where instead of scraping
away coating on the wireless charging coils, I added a second inductor below the one we needed to 'tap' in series.  Figure 1.3 
is the circuit breadboarded with an arrow pointing to the second inductor.

![](Source.JPG)

Figure 1.2 is the final circuit design for the LC oscillator

![](InkedIMG_2252.jpg)

Figure 1.3 is an image of figure 1.2 breadboarded

To verify our inductor coil was working/had current oscillating across it, we used the digital mutlimeter and found its frequency 
meaining there is an oscillation and waveform across the inductance coil which means it is creating an oscillating current
and therefore has a changing magnetic field around the inductor. This changing field is what will induce the emf in reciever
coil. The multimeter is connected across the range labeled V in the above circuit diagram (figure 1.2). While this is not a good enough
indicator yet of performance it is a nice validation until I can verify on the oscilloscope.

Next, I will breadboard the reciever circuit and put a notch in the inductor coil so I don't need to use both our inductor 
coils in place of one (at this point in time we only have two coils, a possible oversight especially for testing and verification)
especially since I need the second one to put in the reciever circuit. I need to find the polarized 
capacitors on the meantime since it says they are delivered but are missing in the box.

Now that capacitors are here I can start working on breadboarding the reciever circuit next and then verify 
power delivery and work from there with David and Arely to attach the load and motor and make sure it all works in tandem. 
Capacitors are polarized which is not something the 445 lab itself has (I later found this was not the case). 
These capacitors also did not have feet for breadboarding so I had to solder on wires to the 10uF and 100uF and finished breadboarding 
reciever circuit in accordance to figure 1.4 depicted below.

![](Sink.JPG)

Figure 1.4 is the power sink a.k.a. power reciever circuit that will connect its output
to the operational components such as the motor, processor, etc.

=======================================.

=======================================.

Date: 4/6/2023

Topics Covered: Door picked up from ECEShop, power circuits both prototyped

Notes:

Earlier today I picked up the door with a motor mounted to the deadbolt lock from the ECE machine shop downstairs.
It is not exactly what I had in mind  but we are getting very close to the deadline and I know they have other projects
so they won't have time to discuss and update the design and it is good enough for now in terms of proof of concept.
The door has had the deadbolt handle removed from the inside. Our proposal focused on the fact that the door should
be still usable even when our motor / housing is attached and this is the opposite since it is only usable
with our motor. 

Now that the voltage regulators and induction coils came in I was also able to breadboard the power reciever circuit.
Using a soldering iron I was able to attach wires to each foot of the voltage regulator.
This breadboarded circuit is shown below, and at this point in time of this testing
the battery and diode were added later but the red wire output from the full
bridge went directly into the red wire input of the voltage regulator. The battery and diode from its positive terminal
were added later.
![](IMG_2251.JPG)

Figure 1.5 is the finalized breadboarded power reciever circuit depicted in 1.4

I emulated an input /induced emf from an induction coil by connecting the positive and negative of a 
DC power supply across the power recievers induction coil. A more realistic test would be to connect
a waveform generator across this coil since the coil would have an oscillating emf. The waveform
generator at my bench was broken and realistically would be turned into DC voltage.
The one thing missing from this test is the fact that even a DC rectified waveform
would have its the dips in a sinusoidal waveform and 'drops' in power delivered.
This test has the side effect of charging up each capacitor in the power reciever circuit, 
and capacitors take time to discharge especially when polarized so making before re-running this test
if necessary, make sure to connect a wire from positive voltage of each capacitor to ground to discharge them for the
circuit to be at a proper point to run the test form start.

Using the DC power supply test described above, I ran the DC source at 6V and pulled around .6 A. This would charge up
the capacitors in the power reciever circuit but they were not reaching the 5V+ required at the input of the regulator for it
to begin working.


Instead of tapping the inductor coil by putting a notch into the coating previously mentioned, I just used a backup induction 
coil to act as a top and bottom half with the center simulating a point where the notch would be.

Putting the two together proves to be a bit of an issue, I have not fully confirmed the power transmitter circuit to be working
after finishing the power reciever circuit and it may have gotten damaged when being moved around the locker. 
Next time I will confirm this part works on its own and then bring the two together to confirm a 5V dc output for the rest of 
our circuit components.

=======================================.

=======================================.

Date: 4/11/2023

Topics Covered: Power circuit testing

Notes:

Putting them together I was able to confirm ossiclation on the oscilloscope. 
I measured voltage across both induction coils independently and was able to confirm oscillation from hi to lo voltages.
I was testing different input currents and voltages to the power transmission circuit so these values also changed with inputs.
The final plan for this product is to power this power transmission circuit with a rechargeable battery but being this close
to the end while still testing out different inputs for power its likely not possible we will be able to find and order
a rechargeable battery suitable for the needs of this circuit in time.

![](WaveForm.JPG)

Figure 1.6 is an oscillating waveform across the oscilloscope

![](IMG_2207.JPG)

Figure 1.7 is the power transmission circuit with the oscilloscope probes connected

On the transciever circuit I am only getting 1.7 volts at the output of the full bridge rectifier (which is also the input to 
the voltage regulator). This means voltage regulator is not able to output any voltage at its output since it requires 5V 
minimum to output 5V. Current ideas are to use an op amp to possibly boost signal. I think there is a suitable one at the ECE
store but they are closed at this time.

=======================================.

=======================================.

Date: 4/12/2023

Topics Covered: Power circuit testing cont.

Notes:

Went to ECE store early morning but the op amp I was looking at on the ECE store site was not there. I believe I was looking at
an outdated catalog. I am still not sure exactly why I was only hitting 1.7 V at the output of the full bridge rectifier.
Since I was at the ECE shop I got myself a 1uH inductor thinking possibly the lack of voltage in the recieve was due to the improvised 
induction tap I had in the power transmission circuit wasn't very optimal. Since it was in fact just two induction coils in series with 
the center acting as the tap, the inductance meant a large voltage was also dropping across the second coil which was not actually the 
same size as the reciever coil so I was not using it to transmit power.

Returning to this point, the voltage across the induction coil should not matter in terms of induced emf. The induced emf is based
on the turn ratio, current through the induction coil, coil area, as well as 'switching rate' or the speed of the oscillations.
I thought this meant it was just wasted power through this coil and using a smaller inductor in series to act as the 'tap' to
divert this power to mostly transmit across the top half of the tap which was used to induce 
a higher emf in the reciever circuit but I was mistaken.

Still, I was getting 1.7 volts at the output of the full bridge so I had an idea to use battery as a voltage boost to hit that 5V I found a
3.8 V battery on the ECE store site but again they were closed at this time. This battery is not rechargeable so I added it in series
between the output of the full bridge and input of the voltage regulator so this 3.8 battery should get me up to 5.5 V when 
added in series with the 1.7 V which is enough to get that voltage regulator output working. I don't want any current running 
back through the battery from + to - terminal to discharge through the capacitors connecting its negative terminal to ground 
so I am going to pick up another diode to put on the end of it to direct current flow in the proper direction. This is where the
battery and diode addition in figure 1.5 comes from.

=======================================.

=======================================.

Date: 4/13/2023

Topics Covered: Power circuit testing cont.

Notes:

I picked up the battery and diode today and attached it in series between the full bridge and regulator input as previously
detailed but even with a 3.8V in series with the previous 1.7V node, I was still only getting 1.7. At first I thought it was possibly
due to the capacitors connecting the battery negative voltage/ output of the full bridge to ground so I added resistors here to boost
that 1.7 volts up but these didn't do anything. This confirmed to me that the issue was now with my voltage regulator.
I reread through the voltage regulator datasheet and noticed on some of the diagrams they specified a polarized capacitance to ground
since the voltage regulator needs a direct connection to ground as a reference and discharge. Adding in an extra polarized capacitor
I had as well as putting two more non-polarized in parallel (This took some testing and I ended up with single 1uH polarized capacitor)
I was finally able to get 5V at the input of the voltage regulator. 
Since the regulator output was connected to ground but had nothing to build a voltage difference to ground here therefore input wasn't able to
 maintain a high voltage and actually dropped due to the voltage maximum across the regulator.
I have not yet tested voltage regulator output in terms of power being output to confirm it will be able to power all components
but that is next on the to do list.

=======================================.

=======================================.

Date: 4/18/2023

Topics Covered: Mock Demo and to do list

Notes:

Today was the mock demo with Nikhil. I was able to demonstrate the circuit output hitting 5 volts/voltage regulator hitting 5 volts on output.
Since I have been using purely the DC power supply on the test bench up to this point he informed me that during the final demo there are points
off for using the bench as a power supply. I also needed more numbers on circuits power characteristics. One of our high level requirements
was short term power after the power supply had been moved away. He also notified me that since we couldn't use the bench, any waveforms on the
oscilloscope using in testing should be saved/ to take pictures of them in order to show during our final demo since we are unable to truly
show it on the oscilloscope since we can't use the bench.
I had spent around 3-4 hours today getting the circuit back in working order and running tests. It took around 30 seconds for the output of the 
voltage regulator to hit 5 volts which was an issue since we do not want a user to have to hold their RFID card/battery pack to the
power reciever circuit for 30 seconds before the circuit even began working.
Also, my circuit design has been using a 3.6 V battery as a step up voltage as previously mentioned and my solution to implement this battery into
the circuit was crude at best (two wires taped to the ends of a battery). This needs to change and my circuit needs to be cleaned up for the final
demo. 
We also need to print out circuit diagram and block diagram for the final demo so I will work on the circuit diagram to be able to print it out
since the circuit has undergone some changes due to testing since the initial circuit idea.
=======================================.
=======================================.
Date: 4/21/2023

Topics Covered: Power testing

Notes:

Circuit was in a working state today despite my makeshift battery implementation. I ordered a piece to slot the battery into which had positive and
negative wires to attach back into my breadboard. I tried a new voltage on the DC power supply since previously it was outputting 17 V and .44
amps which is a non-standard voltage and since we need to move away from the bench top for the final demo, I tried 9V. The circuit worked at 9V
and was drawing .132 Amps at the power transmitter circuit. 
Now that I had a stable voltage to work at, I ordered the 9V battery, a housing for it with wires to connect to the circuit and began testing
power characteristics. First was how long it took for the power reciever to hit 5V at the voltage regulator so it could start operating.
I rand time trails with the 9V .132 A and measured voltage at the input of the voltage regulator and results are given below.

![](Trial_1.JPG)

Figure 1.8 Shows the trial times for charging the voltage regulator up to 5V

Testing procedures for this were similar to the previous testing done with the DC power supply. I connected the power transmission circuit
to the 9V battery housing which had an on/off switch. The initial outliers (trials 1 and 2) in the testing were due to the fact the induction 
coils were not aligned so make sure to align the coils. Also connecting wires from positive sides of capacitors to ground discharges them
and the circuit starts from rest. Next, connect the multimeter to measure DC voltage to the voltage regulator. Starting a timer as soon
as you flip the 9V housing to on (which connects the 9V to the rest of the circuit) you can measure the time it takes for the regulator
voltage to hit its operating voltage (5V) and then stop the timer. These times were then measured. To reset this trial, turn off the 9V and
again discharge the capacitors to get the circuit back to neutral.


Starting these time trials, trial 1 and 2 were taking around 30 seconds for the regulator to hit 5 V which was previously the case but since I had been working on the circuit I believed I had lowered this time due to swapping out some capacitors for lower value capacitance to allow them to 
charge more quickly and the voltage would build quicker. I realigned the induction coils together and then continued running time trails.
The lowest charging time from trials 3-10 was 3.75 seconds and a high of 6.86. While not instantaneous the circuit gets powered much more quickly
now and allows the RFID scanner and microprocessor to do their thing. 

Another high level requirement we enumerated in our design doc is short term power after removing the power supply away from the door lock. A user
will unlock the door by placing the power transmission circuit near the power reciever and the RFID scanner scans the RFID tag simultaneously.
After this a user will remove the power source and leave the circuit without any active power transmission. Therefore we were looking to add short
term power to the circuit to allow it to operate even after a user had removed the power supply from it since it may have more operations to do 
such as play an unlock sound, light an LED, relock the door etc.

I began running more trails on how long the voltage regulator could maintain a 5V output after the power supply have been removed. If a user removes
the power supply right as the circuit hits 5V at the regulator, there is no significant charge/power stored in any capacitors so testing how long 
the voltage regulator stays on after the power supply is removed imediately is pointless as it will immedeiately drop below 5V and the circuit 
will cease to work. In that case, I ran trials on how long the circuit could stay powered after the power had been connected for some time. 
This time was decided arbitrarily as 10 seconds after the regulator hit 5V since this would mean a user would hold their card to the unit for 
around 15 or so seconds. These time trials resulted in the voltage regulator outputting power at 5V for another ~30 seconds.
The initial set up is the same as the previous trial, 9V in off position, coils aligned and capacitors discharged. This time you only start the timer
when the regulator hits 5V. Waiting exactly 10 seconds after the point the regultor hits 5V you will turn the timer on and turn the 9V off simultaneously.
This now begins the discharge time of the capacitors that power the circuit even when the power (9V battery) is removed. These time trials are shown below.


![](Trial2.JPG)

Figure 1.9 Shows the time trials for dropout voltage

TODO's: Create new circuit diagram for final demo print out, test circuit with 9V battery as battery source, determine how much power is drawn from battery per use (.132 A for around 15 seconds), finally power output from voltage regulator.

=======================================.

=======================================.

Date: 04/22/2023

Topics Covered: Final circuit prep for demo

Notes:

As previously mentioned, the circuit needed to be prepped to move away from the bench. Today, I went back in and double
checked the circuit works at 9V again and it runs at 9V drawing .132 amps. Now that I confirmed the circuit was still working
properly using power supply I put the batteries into their casings, stipped wires and soldered them properly to put the 
batteries in the circuit. Then I again confirmed the circuit was working and it was pulling 9V at .132 amps. The circuit 
runs at these conditions for around ~15 seconds for the circuit to gain enough power to operate even after power supply is removed.
The 9V has a 500 mAh capacity when running at ~100 mA as previously stated it is running at (132 mA). 
9V Battery datasheet (https://data.energizer.com/pdfs/max-eu-9v.pdf). 
So a 500 mAh capacity translates to (500*60*60) to get to mAseconds. 1,800,000 mAseconds total battery capacity. If we run at
132 mA for 15 seconds = 132*15 = 1980 mAseconds on average per door unlock. The 9V battery will need to be recharged after 
every 900 uses on average.

I put a 47 ohm resistor at the end of the full bridge rectifier to check power transmitted by the induction coils. It was only
hitting .1 A which was not enough current for the circuit to work under regular operating conditions where the 3.6 V battery also came
in handy since it acts as a helper. This kills one of the high level requirements since this battery will need to be replaced. On 
average this battery is supplying an extra .3745 Amps (Ioperating -.1). The 3.6 V battery runs at 2300 mAh or 8,280,000  and
.3745 A for 15 seconds which comes out to 5617.5 mAseconds per use and a total of ~1474 uses before the battery needs to be replaced.
Again, not ideal since we were setting out to create a circuit that would not require any battery replacements but 1,500 uses is a 
long term life and creating an ease of access into the unit for replacing this battery should be relatively easy but is a mechanical
design component that would be considered during development of the products casing.
3.6 V Battery Datasheet (https://www.kempstoncontrols.com/6ES7971-0BA00/Siemens/sku/76422)

=======================================.


